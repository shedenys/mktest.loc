(function (factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as anonymous module.
    define(['jquery'], factory);
  } else if (typeof exports === 'object') {
    // Node / CommonJS
    factory(require('jquery'));
  } else {
    // Browser globals.
    factory(jQuery);
  }
})(function ($) {

  'use strict';

  var console = window.console || { log: function () {} };

  function CropCropper($element) {
    this.$container = $element;

    this.$cropperView = this.$container.find('.cropper-view');
    this.$cropper = this.$cropperView.find('img');
    this.$cropperModal = this.$container.find('#cropper-modal');
    this.$loading = this.$container.find('.loading');

    this.$cropperForm = this.$cropperModal.find('.cropper-form');
    this.$cropperUpload = this.$cropperForm.find('.cropper-upload');
    this.$cropperSrc = this.$cropperForm.find('.cropper-src');
    this.$cropperData = this.$cropperForm.find('.cropper-data');
    this.$cropperAspectRatio = this.$cropperForm.find('.aspect-ratio');
    this.$cropperInput = this.$cropperForm.find('.cropper-input');
    this.$cropperSave = this.$cropperForm.find('.cropper-save');
    this.$cropperBtns = this.$cropperForm.find('.cropper-btns');

    this.$cropperWrapper = this.$cropperModal.find('.cropper-wrapper');
    this.$cropperPreview = this.$cropperModal.find('.cropper-preview');

    this.init();
  }

  CropCropper.prototype = {
    constructor: CropCropper,

    support: {
      fileList: !!$('<input type="file">').prop('files'),
      blobURLs: !!window.URL && URL.createObjectURL,
      formData: !!window.FormData
    },

    init: function () {
      this.support.datauri = this.support.fileList && this.support.blobURLs;

      if (!this.support.formData) {
        this.initIframe();
      }

      this.initTooltip();
      this.initModal();
      this.addListener();
    },

    addListener: function () {
      this.$cropperView.on('click', $.proxy(this.click, this));
      this.$cropperInput.on('change', $.proxy(this.change, this));
      this.$cropperForm.on('submit', $.proxy(this.submit, this));
      this.$cropperBtns.on('click', $.proxy(this.rotate, this));
    },

    initTooltip: function () {
      this.$cropperView.tooltip({
        placement: 'bottom'
      });
    },

    initModal: function () {
      this.$cropperModal.modal({
        show: false
      });
    },

    initPreview: function () {
      var url = this.$cropper.attr('src');

      this.$cropperPreview.html('<img src="' + url + '">');
    },

    initIframe: function () {
      var target = 'upload-iframe-' + (new Date()).getTime();
      var $iframe = $('<iframe>').attr({
            name: target,
            src: ''
          });
      var _this = this;

      // Ready ifrmae
      $iframe.one('load', function () {

        // respond response
        $iframe.on('load', function () {
          var data;

          try {
            data = $(this).contents().find('body').text();
          } catch (e) {
            console.log(e.message);
          }

          if (data) {
            try {
              data = $.parseJSON(data);
            } catch (e) {
              console.log(e.message);
            }

            _this.submitDone(data);
          } else {
            _this.submitFail('Image upload failed!');
          }

          _this.submitEnd();

        });
      });

      this.$iframe = $iframe;
      this.$cropperForm.attr('target', target).after($iframe.hide());
    },

    click: function () {
      this.$cropperModal.modal('show');
      this.initPreview();
    },

    change: function () {
      var files;
      var file;

      if (this.support.datauri) {
        files = this.$cropperInput.prop('files');

        if (files.length > 0) {
          file = files[0];

          if (this.isImageFile(file)) {
            if (this.url) {
              URL.revokeObjectURL(this.url); // Revoke the old one
            }

            this.url = URL.createObjectURL(file);
            this.startCropper();
          }
        }
      } else {
        file = this.$cropperInput.val();

        if (this.isImageFile(file)) {
          this.syncUpload();
        }
      }
    },

    submit: function () {
      if (!this.$cropperSrc.val() && !this.$cropperInput.val()) {
        return false;
      }

      if (this.support.formData) {
        this.ajaxUpload();
        return false;
      }
    },

    rotate: function (e) {
      var data;

      if (this.active) {
        data = $(e.target).data();

        if (data.method) {
          this.$img.cropper(data.method, data.option);
        }
      }
    },

    isImageFile: function (file) {
      if (file.type) {
        return /^image\/\w+$/.test(file.type);
      } else {
        return /\.(jpg|jpeg|png|gif)$/.test(file);
      }
    },

    startCropper: function () {
      var _this = this;

      if (this.active) {
        this.$img.cropper('replace', this.url);
      } else {
        this.$img = $('<img src="' + this.url + '">');
        this.$cropperWrapper.empty().html(this.$img);
        this.$img.cropper({
          aspectRatio: this.$cropperAspectRatio.val(),
          preview: this.$cropperPreview.selector,
          strict: false,
          crop: function (e) {
            var json = [
                  '{"x":' + e.x,
                  '"y":' + e.y,
                  '"height":' + e.height,
                  '"width":' + e.width,
                  '"rotate":' + e.rotate + '}'
                ].join();

            _this.$cropperData.val(json);
          }
        });

        this.active = true;
      }

      this.$cropperModal.one('hidden.bs.modal', function () {
        _this.$cropperPreview.empty();
        _this.stopCropper();
      });
    },

    stopCropper: function () {
      if (this.active) {
        this.$img.cropper('destroy');
        this.$img.remove();
        this.active = false;
      }
    },

    ajaxUpload: function () {
      var url = this.$cropperForm.attr('action');
      var data = new FormData(this.$cropperForm[0]);
      var _this = this;

      $.ajax(url, {
        type: 'post',
        data: data,
        dataType: 'json',
        processData: false,
        contentType: false,

        beforeSend: function () {
          _this.submitStart();
        },

        success: function (data) {
          _this.submitDone(data);
        },

        error: function (XMLHttpRequest, textStatus, errorThrown) {
          _this.submitFail(textStatus || errorThrown);
        },

        complete: function () {
          _this.submitEnd();
        }
      });
    },

    syncUpload: function () {
      this.$cropperSave.click();
    },

    submitStart: function () {
      this.$loading.fadeIn();
    },

    submitDone: function (data) {
      console.log(data);
      // $('#cropper-input').val(data.file_name);
      // $('#cropper-extension').val(data.file_extension);
      $('#cropper-input').val(data.file_name + '.' + data.file_extension);

      if ($.isPlainObject(data) && data.state === 200) {
        if (data.result) {
          this.url = data.result;

          if (this.support.datauri || this.uploaded) {
            this.uploaded = false;
            this.cropDone();
          } else {
            this.uploaded = true;
            this.$cropperSrc.val(this.url);
            this.startCropper();
          }

          this.$cropperInput.val('');
        } else if (data.message) {
          this.alert(data.message);
        }
      } else {
        this.alert('Failed to response');
      }
    },

    submitFail: function (msg) {
      this.alert(msg);
    },

    submitEnd: function () {
      this.$loading.fadeOut();
    },

    cropDone: function () {
      this.$cropperForm.get(0).reset();
      this.$cropper.attr('src', '/' + this.url);
      this.stopCropper();
      this.$cropperModal.modal('hide');
    },

    alert: function (msg) {
      var $alert = [
            '<div class="alert alert-danger cropper-alert alert-dismissable">',
              '<button type="button" class="close" data-dismiss="alert">&times;</button>',
              msg,
            '</div>'
          ].join('');

      this.$cropperUpload.after($alert);
    }
  };

  $(function () {
    return new CropCropper($('#crop-cropper'));
  });

});
