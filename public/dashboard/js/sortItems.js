$('.tb-sort').click(function(e){
	e.preventDefault();

	var sort = $(this).attr('data-sort')
		order = $(this).attr('data-order');

	var queryParameters = {},
		queryString = location.search.substring(1),
	    re = /([^&=]+)=([^&]*)/g, m;

	// Creates a map with the query string parameters
	while (m = re.exec(queryString)) {
	    queryParameters[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
	}

	// Add new parameters or update existing ones

	if(order == '') {
		// Delete existing sort
		delete queryParameters['sort['+sort+']'];
	}
	else {
		var sorting = {};
		sorting[sort] = order;

		if(queryParameters['sort['+sort+']'] != undefined) {
			// Update existing sort
			delete queryParameters['sort['+sort+']'];
			queryParameters['sort'] = sorting;
		}
		else {
			// Add new sort
			queryParameters['sort'] = sorting;

		}

	}
	
	location.search = $.param(queryParameters);
});