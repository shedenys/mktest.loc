$(function() {
  $('#scrollToTop').hide();
  $('#scrollToTop').css('opacity', '1');
  $(window).scroll(function() {
    if($(this).scrollTop() != 0) {
      $('#scrollToTop').removeClass('slideOutDown');
      $('#scrollToTop').addClass('slideInUp');
      $('#scrollToTop').show();
    } else {
      $('#scrollToTop').removeClass('slideInUp');
      $('#scrollToTop').addClass('slideOutDown');
      $('#scrollToTop').fadeOut();
    }
  });
  $('#scrollToTop').click(function(e) {
    e.preventDefault();
    $('body,html').animate({scrollTop:0},800);
  });
});