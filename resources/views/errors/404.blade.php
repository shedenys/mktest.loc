<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Страница не найдена - {{ Request::server('HTTP_HOST') }}</title>
    <meta name="msapplication-TileColor" content="#9f00a7">
    <meta name="msapplication-TileImage" content="/dashboard/img/favicon/mstile-144x144.png">
    <meta name="msapplication-config" content="/dashboard/img/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <link rel="apple-touch-icon" sizes="57x57" href="/dashboard/img/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/dashboard/img/favicon/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/dashboard/img/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/dashboard/img/favicon/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/dashboard/img/favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/dashboard/img/favicon/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/dashboard/img/favicon/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/dashboard/img/favicon/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/dashboard/img/favicon/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/dashboard/img/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/dashboard/img/favicon/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/dashboard/img/favicon/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/dashboard/img/favicon/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/dashboard/img/favicon/manifest.json">
    <link rel="shortcut icon" href="/dashboard/img/favicon/favicon.ico">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>  <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>  <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>  <![endif]-->
    <link href="/dashboard/css/vendors.min.css" rel="stylesheet" />
    <link href="/dashboard/css/animate.min.css" rel="stylesheet" />
    <link href="/dashboard/css/styles.min.css" rel="stylesheet" />
  </head>
  <body class="page-error">
    <div class="center">
      <div class="card bordered z-depth-2 animated bounceIn" style="margin:0% auto; max-width:400px;">
        <div class="card-content">
          <div class="m-b-30 text-center"> <i class="md md-cancel error-icon"></i>
            <h1 class="pink-text uppercase">404</h1>
            <p class="card-title-desc">Мы сожалеем, но страница, которая Вам нужна, больше не существует.</p>
          </div>
        </div>
        <div class="card-action clearfix">
          <div class="text-center"> <a href="/" class="btn btn-primary btn-block">Перейти на главную страницу</a> </div>
        </div>
      </div>
    </div>
    
    <script charset="utf-8" src="/dashboard/js/vendors.min.js"></script>
    <script charset="utf-8" src="/dashboard/js/app.min.js"></script>
  </body>
</html>