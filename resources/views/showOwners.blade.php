@extends('layout')

@section('main')
  @include('partials.pageHead', ['title' => $ccTitle, 'description' => 'Здесь Вы можете добавлять, редактировать, и удалять владельцев.', 'iconClass' => 'md md-account-circle'])
  <div>
    @if($ownersTotal > 0)
      <div class="table-responsive well no-padding white no-margin">
        <h3 class="table-title">Владельцев: {{ $ownersTotal }}</h3>
        {!! Form::open(['class' => 'items']) !!}
          {!! Form::hidden('action') !!}
          <table class="table table-full" id="table-area-1" fsm-big-data="data of data take 30">
            <thead>
              <tr fsm-sticky-header="" scroll-body="'#table-area-1'" scroll-stop="64">
                <th style="width: 78px;">
                  <input type="checkbox" class="relative" id="checkAllItems">
                </th>
                <th style="width: 51px;"></th>
                @include('partials.dataTablesSortingTH', ['sort' => $sort, 'column' => 'name', 'label' => 'ФИО'])
                @include('partials.dataTablesSortingTH', ['sort' => $sort, 'column' => 'birthday', 'label' => 'Дата рождения'])
                <th>Домашний адрес</th>
                <th>Телефон</th>
                <th class="text-right">Действия</th>
              </tr>
            </thead>
            <tbody>
              @foreach($owners as $owner)
                <tr>
                  <td>
                    <input type="checkbox" name="items[]" value="{{ $owner->id }}" class="relative">
                  </td>
                  <td>
                    <i class="md md-account-circle teal accent-4 icon-color"></i>
                  </td>
                  <td>{{ $owner->name }}</td>
                  <td>{{ $owner->birthday }}</td>
                  <td>{{ $owner->home_address }}</td>
                  <td>{{ $owner->phone }}</td>
                  <td class="text-right">
                    <a href="owners/edit/{{ $owner->id }}" class="btn btn-link btn-round" data-title="Редактировать" data-toggle="tooltip"><i class="md md-edit"></i></a>
                    <a href="#" class="btn btn-link btn-round btn-remove" data-id="{{ $owner->id }}" data-title="Удалить" data-toggle="tooltip"><i class="md md-delete"></i></a>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        {!! Form::close() !!}
        <div class="col-sm-5">
          <div class="dataTables_info" id="example_info" role="status" aria-live="polite">
            @include('partials.dataTablesInfo', ['items' => $owners, 'total' => $ownersTotal])
          </div>
        </div>
        @if($ownersTotal > 0)
          <div class="col-sm-7">
            <div class="dataTables_paginate">
            {!! $owners->render() !!}
            </div>
          </div>
        @endif
      </div>
    @endif

    @if($ownersTotal == 0)
      <h3 class="table-title">Нет добавленных владельцев.</h3>
    @endif

    <div class="footer-buttons">
      <div class="btn btn-default btn-round btn-lg m-r-10 animated" id="scrollToTop" data-title="Прокрутить вверх" data-toggle="tooltip"><i class="md md-arrow-drop-up"></i></div>
      <div class="btn btn-primary btn-round btn-lg m-r-10 btn-footer btn-group-remove" data-title="Удалить выбранных владельцев" data-toggle="tooltip"><i class="md md-delete"></i></div>
      <a href="/owners/add" class="btn btn-primary btn-round btn-lg" data-title="Добавить владельца" data-toggle="tooltip"><i class="md md-add"></i></a>
    </div>
  </div>
@endsection

@section('scripts')
    <script src="/dashboard/js/deleteItems.js"></script>
    <script src="/dashboard/js/sortItems.js"></script>
    <script src="/dashboard/js/scrollToTop.js"></script>
@endsection

@section('styles')
  {{-- Для кнопки прокрутки вверх --}}
  <link href="/dashboard/css/animate.min.css" rel="stylesheet" />
@endsection