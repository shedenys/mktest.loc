@extends('layout')

@section('main')
  @include('partials.pageHead', ['title' => $ccTitle, 'description' => 'Здесь Вы можете управлять информацией пользователя.', 'iconClass' => 'md md-account-child'])
  <section class="forms-basic">
    <div class="row  m-b-40">
      <div class="col-md-12">
        <div class="well white" id="forms-validation-container">
          {!! Form::model($user, ['class' => 'form-floating', 'id' => 'form-validation' ]) !!}
            <fieldset>
              <div class="form-group"></div>
                <legend>Информация</legend>
                <div class="form-group">
                  <label class="control-label">Имя</label>
                  {!! Form::text('name', $user->name, ['class'=>'form-control', 'required' => 'required', 'data-error' => 'Введите имя']) !!}
                  <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                  <label class="control-label">E-mail</label>
                  {!! Form::email('email', $user->email, ['class'=>'form-control', 'required' => 'required', 'data-error' => 'Введите email']) !!}
                  <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                  <label class="control-label">Пароль</label>
                  <input type="password" name="password" class="form-control" @if(!Request::segment(4)) required="required" data-error="Введите пароль"@endif>
                  @if(!Request::segment(4))
                    <div class="help-block with-errors"></div>
                  @endif
                </div>
                <div class="form-group">
                  <label class="control-label">Подтвердите пароль</label>
                  <input type="password" name="password_confirmation" class="form-control" @if(!Request::segment(4)) required="required" data-error="Подтвердите пароль"@endif>
                  @if(!Request::segment(4))
                    <div class="help-block with-errors"></div>
                  @endif
                </div>
                <div class="form-group">
                  <label class="control-label">Уровень прав</label>
                  {!! Form::text(null, $user->role, ['class'=>'form-control', 'readonly' => 'readonly']) !!}
                </div>
            </fieldset>
          {!! Form::close() !!}
          <div class="form-group">
            <button type="submit" class="btn btn-primary" form="form-validation">Сохранить</button>
            <button class="btn btn-default btn-cancel" onclick="window.history.back();">Отмена</button>
          </div>
        </div>
      </div>
    </div>
  </section>
@stop