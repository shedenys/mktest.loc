@extends('auth.layout')

@section('main')
  <div class="card bordered z-depth-2 @if(!count($errors) && !session('status'))animated fadeIn @endif" style="margin:0% auto; max-width:400px;">
    <div class="card-header">
      <div class="brand-logo">Сброс пароля</div>
    </div>
    <div id="forms-validation-container">
      <form class="form-floating" id="form-validation" method="POST" action="/password/reset">
        <div class="card-content">
          @if(session('status'))
            <div class="m-b-30">
              <div class="bs-component">
                <div class="alert alert-dismissible alert-success">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <h4>Готово!</h4>
                  <p>{{ session('status') }}</p>
                </div>
              </div>
            </div>
          @endif
          @if(count($errors) > 0)
            <div class="m-b-30">
              <div class="bs-component">
                <div class="alert alert-dismissible alert-danger">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <h4>Ошибка</h4>
                  @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                  @endforeach
                </div>
              </div>
            </div>
          @endif
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="token" value="{{ $token }}">
          <div class="form-group">
            <label for="inputEmail" class="control-label">E-mail</label>
            <input type="email" class="form-control" name="email" value="{{ old('email') }}" required="" data-error="Введите корректный e-mail адрес">
            <div class="help-block with-errors"></div>
          </div>
          <div class="form-group">
            <label for="inputPassword" class="control-label">Новый пароль</label>
            <input type="password" class="form-control" id="inputPassword" name="password" required="" data-error="Введите пароль">
            <div class="help-block with-errors"></div>
          </div>
          <div class="form-group">
            <label for="inputPasswordConfirmation" class="control-label">Повторите пароль</label>
            <input type="password" class="form-control" id="inputPasswordConfirmation" name="password_confirmation" required="" data-error="Повторите пароль">
            <div class="help-block with-errors"></div>
          </div>
        </div>
        <div class="card-action clearfix">
          <div class="pull-right">
            <button type="submit" class="btn btn-link black-text">Сбросить пароль</button>
          </div>
        </div>
      </form>
    </div>
  </div>
@stop