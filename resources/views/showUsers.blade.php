@extends('layout')

@section('main')
  @include('partials.pageHead', ['title' => $ccTitle, 'description' => 'Здесь Вы можете добавлять, редактировать, и удалять пользователей, которым разрешен доступ в административную часть сайта.', 'iconClass' => 'md md-account-child'])
  <div>
    @if($usersTotal > 0)
      <div class="table-responsive well no-padding white no-margin">
        <h3 class="table-title">Пользователей: {{ $usersTotal }}</h3>
        {!! Form::open(['class' => 'items']) !!}
          {!! Form::hidden('action') !!}
          <table class="table table-full" id="table-area-1" fsm-big-data="data of data take 30">
            <thead>
              <tr fsm-sticky-header="" scroll-body="'#table-area-1'" scroll-stop="64">
                <th style="width: 78px;">
                  <input type="checkbox" class="relative" id="checkAllItems">
                </th>
                <th style="width: 51px;"></th>
                @include('partials.dataTablesSortingTH', ['sort' => $sort, 'column' => 'name', 'label' => 'Имя'])
                @include('partials.dataTablesSortingTH', ['sort' => $sort, 'column' => 'role', 'label' => 'Уровень прав'])
                @include('partials.dataTablesSortingTH', ['sort' => $sort, 'column' => 'created_at', 'label' => 'Зарегистрирован'])
                @include('partials.dataTablesSortingTH', ['sort' => $sort, 'column' => 'updated_at', 'label' => 'Обновлено'])
                <th class="text-right">Действия</th>
              </tr>
            </thead>
            <tbody>
              @foreach($users as $user)
                <tr>
                  <td>
                    <input type="checkbox" name="items[]" value="{{ $user->id }}" class="relative">
                  </td>
                  <td><i class="md md-account-child red darken-1 icon-color"></i></td>
                  <td>{{ $user->name }}</td>
                  <td>{{ $user->role }}</td>
                  <td>{{ $user->created_at->format('d.m.Y, H:i') }}</td>
                  <td>{{ $user->updated_at->format('d.m.Y, H:i') }}</td>
                  <td class="text-right">
                    {{-- <a href="#" class="btn btn-link btn-round" data-title="Не отображать" data-toggle="tooltip" style="color: silver;"><i class="md md-remove-red-eye"></i></a> --}}
                    {{-- <a href="#" class="btn btn-link btn-round" data-title="Отображать" data-toggle="tooltip"><i class="md md-remove-red-eye"></i></a> --}}
                    <a href="users/edit/{{ $user->id }}" class="btn btn-link btn-round" data-title="Редактировать" data-toggle="tooltip"><i class="md md-edit"></i></a>
                    <a href="#" class="btn btn-link btn-round btn-remove" data-id="{{ $user->id }}" data-title="Удалить" data-toggle="tooltip"><i class="md md-delete"></i></a>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        {!! Form::close() !!}
        <div class="col-sm-5">
          <div class="dataTables_info" id="example_info" role="status" aria-live="polite">
            @include('partials.dataTablesInfo', ['items' => $users, 'total' => $usersTotal])
          </div>
        </div>
        @if($usersTotal > 0)
          <div class="col-sm-7">
            <div class="dataTables_paginate">
            {!! $users->render() !!}
            </div>
          </div>
        @endif
      </div>
    @endif

    @if($usersTotal == 0)
      <h3 class="table-title">Нет зарегистрированных пользователей.</h3>
    @endif

    <div class="footer-buttons">
      <div class="btn btn-default btn-round btn-lg m-r-10 animated" id="scrollToTop" data-title="Прокрутить вверх" data-toggle="tooltip"><i class="md md-arrow-drop-up"></i></div>
      <div class="btn btn-primary btn-round btn-lg m-r-10 btn-footer btn-group-remove" data-title="Удалить выбранных пользователей" data-toggle="tooltip"><i class="md md-delete"></i></div>
      <a href="/users/add" class="btn btn-primary btn-round btn-lg" data-title="Добавить пользователя" data-toggle="tooltip"><i class="md md-add"></i></a>
    </div>
  </div>
@endsection

@section('scripts')
  <script src="/dashboard/js/deleteItems.js"></script>
  <script src="/dashboard/js/sortItems.js"></script>
  <script src="/dashboard/js/scrollToTop.js"></script>
@endsection

@section('styles')
  {{-- Для кнопки прокрутки вверх --}}
  <link href="/dashboard/css/animate.min.css" rel="stylesheet" />
@endsection