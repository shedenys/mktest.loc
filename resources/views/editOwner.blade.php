@extends('layout')

@section('main')
  @include('partials.pageHead', ['title' => $ccTitle, 'description' => 'Здесь Вы можете указать данные владельца.', 'iconClass' => 'md md-account-circle'])
  <section class="forms-basic">
    <div class="row  m-b-40">
      <div class="col-md-12">
        <div class="well white" id="forms-validation-container">
          {!! Form::model($owner, ['class' => 'form-floating', 'id' => 'form-validation' ]) !!}
            <fieldset>
              <!-- Tab panes -->
              <div role="tabpanel" class="tab-pane active" id="tab-info">
                <div class="form-group"></div>
                <div class="form-group">
                  <label class="control-label">ФИО</label>
                  {!! Form::text('name', $owner->name, ['class'=>'form-control', 'required' => 'required', 'data-error' => 'Введите ФИО']) !!}
                  <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                  <label class="control-label">Дата рождения</label>
                  {!! Form::text('birthday', $owner->birthday, ['class'=>'form-control datepicker', 'required' => 'required', 'data-error' => 'Введите дату рождения']) !!}
                  <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                  <label class="control-label">Домашний адрес</label>
                  {!! Form::text('home_address', $owner->home_address, ['class'=>'form-control', 'required' => 'required', 'data-error' => 'Введите домашний адрес']) !!}
                  <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                  <label class="control-label">Номер телефона</label>
                  {!! Form::tel('phone', $owner->phone, ['class'=>'form-control', 'required' => 'required', 'data-error' => 'Введите номер телефона']) !!}
                  <div class="help-block with-errors"></div>
                </div>
              </div>
            </fieldset>
          {!! Form::close() !!}
          <br>
          <div class="form-group">
            <button type="submit" class="btn btn-primary" form="form-validation">Сохранить</button>
            <button class="btn btn-default btn-cancel" onclick="window.history.back();">Отмена</button>
          </div>
        </div>
      </div>
    </div>
  </section>
@stop

@section('scripts')
  <script src="/dashboard/js/maskedinput.min.js"></script>
  <script type="text/javascript">
    {{-- Указываем формат даты, в котором сохраняет MySQL --}}
    $('.datepicker').datetimepicker({
      format: 'YYYY-MM-DD'
    });
    $("input[type=tel]").mask("+38 (099) 999-99-99");
  </script>
@endsection