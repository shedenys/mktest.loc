<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="msapplication-TileColor" content="#9f00a7">
    <meta name="msapplication-TileImage" content="/dashboard/img/favicon/mstile-144x144.png">
    <meta name="msapplication-config" content="/dashboard/img/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <link rel="apple-touch-icon" sizes="57x57" href="/dashboard/img/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/dashboard/img/favicon/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/dashboard/img/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/dashboard/img/favicon/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/dashboard/img/favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/dashboard/img/favicon/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/dashboard/img/favicon/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/dashboard/img/favicon/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/dashboard/img/favicon/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/dashboard/img/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/dashboard/img/favicon/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/dashboard/img/favicon/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/dashboard/img/favicon/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/dashboard/img/favicon/manifest.json">
    <link rel="shortcut icon" href="/dashboard/img/favicon/favicon.ico">
    <title>{{ $ccTitle }} - {{ $ccServerHttpHost }}</title>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>  <![endif]-->
    <link href="/dashboard/css/vendors.min.css" rel="stylesheet" />
    {{-- <link href="/dashboard/css/styles.min.css" rel="stylesheet" /> --}}
    <link href="/dashboard/css/styles.min.css" rel="stylesheet" />
    <link href="/dashboard/css/additional.css" rel="stylesheet" />
      @section('styles')
      @show
  </head>
  <body scroll-spy="" id="top" class=" theme-template-dark theme-pink alert-open alert-with-mat-grow-top-right">
    <main>
      <aside class="sidebar fixed" style="width: 260px; left: 0px; ">
        <div class="brand-logo">
          <span class="md md-dashboard"></span>
          {{ $ccServerHttpHost }}
        </div>
        <div class="user-logged-in">
          <div class="content">
            <div class="user-name">{{ Auth::user()->name }} <span class="text-muted f9">{{ Auth::user()->role }}</span></div>
            <div class="user-email">{{ Auth::user()->email }}</div>
            <div class="user-actions"> <a class="m-r-5" href="/users/edit/{{ Auth::user()->id }}">аккаунт</a> <a href="/auth/logout">выйти</a> </div>
          </div>
        </div>
        @include('partials.menuItems')
      </aside>
      <div class="main-container">
        <nav class="navbar navbar-default navbar-fixed-top">
          <div class="container-fluid">
            <div class="navbar-header pull-left">
              <button type="button" class="navbar-toggle pull-left m-15" data-activates=".sidebar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              <ul class="breadcrumb">
                {{--*/ $count = 1; /*--}}
                @foreach($ccBreadcrumbs as $breadcrumb)
                  @if($count < count($ccBreadcrumbs))
                    <li><a href="/{{ $breadcrumb['url'] }}">{{ $breadcrumb['name'] }}</a></li>
                  @else
                    <li class="active">{{ $breadcrumb['name'] }}</li>
                  @endif
                  {{--*/ $count++; /*--}}
                @endforeach
              </ul>
            </div>
          </div>
        </nav>
        <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
          @yield('main')
        </div>
      </div>
    </main>
    <style>
    .glyphicon-spin-jcs {
      -webkit-animation: spin 1000ms infinite linear;
      animation: spin 1000ms infinite linear;
    }
    
    @-webkit-keyframes spin {
      0% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
      }
      100% {
        -webkit-transform: rotate(359deg);
        transform: rotate(359deg);
      }
    }
    
    @keyframes spin {
      0% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
      }
      100% {
        -webkit-transform: rotate(359deg);
        transform: rotate(359deg);
      }
    }
    </style>
    
    <script charset="utf-8" src="/dashboard/js/vendors.min.js"></script>
    <script charset="utf-8" src="/dashboard/js/app.min.js"></script>

    @section('scripts')
    @show

  </body>
</html>