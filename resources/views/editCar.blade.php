@extends('layout')

@section('main')
  @include('partials.pageHead', ['title' => $ccTitle, 'description' => 'Здесь Вы можете указать данные владельца.', 'iconClass' => 'md md-account-circle'])
  <section class="forms-basic">
    <div class="row  m-b-40">
      <div class="col-md-12">
        <div class="well white" id="forms-validation-container">
          {!! Form::model($car, ['class' => 'form-floating', 'id' => 'form-validation' ]) !!}
            <fieldset>
              <!-- Tab panes -->
              <div role="tabpanel" class="tab-pane active" id="tab-info">
                <div class="form-group"></div>
                <div class="form-group">
                  <label class="control-label">Марка</label>
                  {!! Form::text('mark', $car->mark, ['class'=>'form-control', 'required' => 'required', 'data-error' => 'Введите название марки']) !!}
                  <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                  <label class="control-label">Модель</label>
                  {!! Form::text('model', $car->model, ['class'=>'form-control', 'required' => 'required', 'data-error' => 'Введите модель']) !!}
                  <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                  <label class="control-label">Год выпуска (в цифрах)</label>
                  {!! Form::number('issue_year', $car->issue_year, ['class'=>'form-control', 'required' => 'required', 'data-error' => 'Введите год выпуска']) !!}
                  <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                  <label class="control-label">Цвет</label>
                  {!! Form::text('color', $car->color, ['class'=>'form-control', 'required' => 'required', 'data-error' => 'Введите цвет']) !!}
                  <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                  <label class="control-label">Номерной знак</label>
                  {!! Form::text('number', $car->number, ['class'=>'form-control', 'required' => 'required', 'data-error' => 'Введите цвет']) !!}
                  <div class="help-block with-errors"></div>
                </div>
                <div class="form-group filled">
                  <label class="control-label">Владелец</label>
                  <select name="owner_id" class="select-owner form-control">
                    <option value="">Нет владельца</option>
                    @foreach($owners as $owner)
                      <option value="{{ $owner->id }}" @if($owner->id == $car->owner_id) selected @endif>{{ $owner->name }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </fieldset>
          {!! Form::close() !!}
          <br>
          <div class="form-group">
            <button type="submit" class="btn btn-primary" form="form-validation">Сохранить</button>
            <button class="btn btn-default btn-cancel" onclick="window.history.back();">Отмена</button>
          </div>
        </div>
      </div>
    </div>
  </section>
@stop

@section('scripts')
  <script>
    owners = [
    { id: '', name: "Нет владельца" },
    @foreach($owners as $owner)
      { id: '{{ $owner->id }}', name: "{{ $owner->name }}" },
    @endforeach
    ];
    templateSelection = function(a) {
      return a.name
    };
    templateResult = function(a) {
      return template = $("<div>" + a.name + "</div>"), template
    };
    $(".select-owner").select2({
      data: owners,
      templateSelection: templateSelection,
      templateResult: templateResult,
      minimumResultsForSearch: 1 / 0
    });
  </script>
@endsection