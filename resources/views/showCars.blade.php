@extends('layout')

@section('main')
  @include('partials.pageHead', ['title' => $ccTitle, 'description' => 'Здесь Вы можете добавлять, редактировать, и удалять автомобили.', 'iconClass' => 'md md-account-circle'])
  <div>
    @if($carsTotal > 0)
      <div class="table-responsive well no-padding white no-margin">
        <h3 class="table-title">Автомобилей: {{ $carsTotal }}</h3>
        {!! Form::open(['class' => 'items']) !!}
          {!! Form::hidden('action') !!}
          <table class="table table-full" id="table-area-1" fsm-big-data="data of data take 30">
            <thead>
              <tr fsm-sticky-header="" scroll-body="'#table-area-1'" scroll-stop="64">
                <th style="width: 78px;">
                  <input type="checkbox" class="relative" id="checkAllItems">
                </th>
                <th style="width: 51px;"></th>
                @include('partials.dataTablesSortingTH', ['sort' => $sort, 'column' => 'name', 'label' => 'Марка'])
                @include('partials.dataTablesSortingTH', ['sort' => $sort, 'column' => 'birthday', 'label' => 'Модель'])
                @include('partials.dataTablesSortingTH', ['sort' => $sort, 'column' => 'birthday', 'label' => 'Год выпуска'])
                @include('partials.dataTablesSortingTH', ['sort' => $sort, 'column' => 'birthday', 'label' => 'Цвет'])
                @include('partials.dataTablesSortingTH', ['sort' => $sort, 'column' => 'birthday', 'label' => 'Номерной знак'])
                @include('partials.dataTablesSortingTH', ['sort' => $sort, 'column' => 'owner_name', 'label' => 'Владелец'])
                <th class="text-right">Действия</th>
              </tr>
            </thead>
            <tbody>
              @foreach($cars as $car)
                <tr>
                  <td>
                    <input type="checkbox" name="items[]" value="{{ $car->id }}" class="relative">
                  </td>
                  <td>
                    <i class="md md-directions-car teal accent-4 icon-color"></i>
                  </td>
                  <td>{{ $car->mark }}</td>
                  <td>{{ $car->model }}</td>
                  <td>{{ $car->issue_year }}</td>
                  <td>{{ $car->color }}</td>
                  <td>{{ $car->number }}</td>
                  <td>@if($car->owner_name){{ $car->owner_name }}@else <span class="t-dummy">Нет владельца</span>@endif</td>
                  <td class="text-right">
                    <a href="edit/{{ $car->id }}" class="btn btn-link btn-round" data-title="Редактировать" data-toggle="tooltip"><i class="md md-edit"></i></a>
                    <a href="#" class="btn btn-link btn-round btn-remove" data-id="{{ $car->id }}" data-title="Удалить" data-toggle="tooltip"><i class="md md-delete"></i></a>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        {!! Form::close() !!}
        <div class="col-sm-5">
          <div class="dataTables_info" id="example_info" role="status" aria-live="polite">
            @include('partials.dataTablesInfo', ['items' => $cars, 'total' => $carsTotal])
          </div>
        </div>
        @if($carsTotal > 0)
          <div class="col-sm-7">
            <div class="dataTables_paginate">
            {!! $cars->render() !!}
            </div>
          </div>
        @endif
      </div>
    @endif

    @if($carsTotal == 0)
      <h3 class="table-title">Нет добавленных автомобилей.</h3>
    @endif

    <div class="footer-buttons">
      <div class="btn btn-default btn-round btn-lg m-r-10 animated" id="scrollToTop" data-title="Прокрутить вверх" data-toggle="tooltip"><i class="md md-arrow-drop-up"></i></div>
      <div class="btn btn-primary btn-round btn-lg m-r-10 btn-footer btn-group-remove" data-title="Удалить выбранные автомобили" data-toggle="tooltip"><i class="md md-delete"></i></div>
      <a href="/add" class="btn btn-primary btn-round btn-lg" data-title="Добавить автомобиль" data-toggle="tooltip"><i class="md md-add"></i></a>
    </div>
  </div>
@endsection

@section('scripts')
    <script src="/dashboard/js/deleteItems.js"></script>
    <script src="/dashboard/js/sortItems.js"></script>
    <script src="/dashboard/js/scrollToTop.js"></script>
@endsection

@section('styles')
  {{-- Для кнопки прокрутки вверх --}}
  <link href="/dashboard/css/animate.min.css" rel="stylesheet" />
@endsection