@if($items->currentPage() == 1)
Отображено с 1 по {{ count($items) }} из {{ $total }} записей
@else
Отображено с {{ $items->perPage() * ($items->currentPage() - 1) + 1 }} по {{ $items->perPage() * ($items->currentPage() - 1) + count($items) }} из {{ $total }} записей
@endif