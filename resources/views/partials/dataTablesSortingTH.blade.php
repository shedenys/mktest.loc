<th class="tb-sort" data-sort="{{ $column }}" data-order="<?php if(isset($sort[$column])) echo $nextOrder[$sort[$column]]; else echo 'asc'; ?>">{{ $label }}&nbsp;
	@if(isset($sort[$column]) && $sort[$column] == 'asc')
		<i class="md md-expand-less"></i>
		<i class="md md-sort"></i>
	@elseif(isset($sort[$column]) && $sort[$column] == 'desc')
		<i class="md md-expand-more"></i>
		<i class="md md-sort"></i>
	@endif
</th>