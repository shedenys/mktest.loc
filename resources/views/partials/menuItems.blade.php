<ul class="menu-links">
  <li><a href="/"><i class="md md-home"></i>&nbsp;<span>Главная</span></a></li>
  <li><a href="/owners"><i class="md md-account-circle"></i>&nbsp;<span>Владельцы</span></a></li>
  @if(Auth::user()->role == 'superadmin')
  <li><a href="/users"@if(in_array(Request::segment(2), ['users'])) class="active"@endif><i class="md md-account-child"></i>&nbsp;<span>Пользователи</span></a></li>
  @endif
</ul>