<section>
	<div class="page-header">
		<h1>
			@if($iconClass)
				<i class="{{ $iconClass }}"></i>&nbsp;
			@endif
			{!! $title !!}
		</h1>
	 	<p class="lead">{!! $description !!}</p>
	</div>
    @if(Session::has('success'))
	    <div class="bs-component">
	        <div class="alert alert-dismissible alert-success">
				<button type="button" class="close" data-dismiss="alert">×</button>
				@if(Session::has('success-title'))
		       		<h4>{{ Session::get('success-title') }}</h4>
				@else
		       		<h4>Сохранено</h4>
				@endif
		       	<p>{{ Session::get('success') }}</p>
		    </div>
		</div>
	@endif
	@if(Session::has('warning'))
	    <div class="bs-component">
	        <div class="alert alert-dismissible alert-warning">
				<button type="button" class="close" data-dismiss="alert">×</button>
				@if(Session::has('warning-title'))
		       		<h4>{{ Session::get('warning-title') }}</h4>
				@else
		       		<h4>Сохранено</h4>
				@endif
		       	<p>{{ Session::get('warning') }}</p>
		    </div>
		</div>
	@endif
	@if(Session::has('error'))
		<div class="bs-component">
	        <div class="alert alert-dismissible alert-danger">
				<button type="button" class="close" data-dismiss="alert">×</button>
		       	<h4>Ошибка</h4>
		       	<p>{{ Session::get('error') }}</p>
		    </div>
		</div>
	@endif
	@if($errors->any())
		<div class="bs-component">
	        <div class="alert alert-dismissible alert-danger">
				<button type="button" class="close" data-dismiss="alert">×</button>
		       	<h4>Ошибка</h4>
				@foreach( $errors->all() as $error )
					<p>{{ $error }}</p>
				@endforeach
		    </div>
		</div>
	@endif
	@if(isset($info['message']))
		<div class="bs-component">
			<div class="alert alert-dismissible alert-info">
				@if(!isset($info['close']) || $info['close'] != false)
					<button type="button" class="close" data-dismiss="alert">×</button>
				@endif
				@if(isset($info['title']))
			 		<h4>{{ $info['title'] }}</h4>
				@endif
			 	<p>{{ $info['message'] }}</p>
			</div>
		</div>
	@endif
</section>	