# Тестовая задание Megakit. Шевченко Денис

ВАЖНО! Приложение требует авторизацию. После развертывания приложения добавьте суперпользователя для входа - сделайте в БД импорт дампа import.sql.
Данные суперпользователя: email - test@test.test, пароль - 123456.

Приложение разработано на Laravel 5.2.45.
Возможности:
1. Добавление/редактиование/удаление автомобиля с указанием владельца и без него. У автомобиля может быть только один владелец (как это утверждено по законодательству).
2. Добавление/редактирование/удаление владельца.
3. Сортировка данных на главной странице по колонкам (по которым есть смысл) и на странице владельцов.
4. Добавлена обработка исключения 404 с отображением соответствующего сообщения.
5. Добавление/редактирование/удаление пользователей (с правами admin) для входа в систему со стороны суперпользователя (права superadmin).
Пользователь с правами admin может редактировать только свой профиль, и не может создавать/удалять других пользователей (как это может superadmin).
6. Адаптивный html-макет.