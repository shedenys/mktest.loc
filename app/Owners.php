<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Owners extends Model
{
    protected $table = 'owners';
    protected $guarded = [];
}
