<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => 'auth'], function() {

	Route::get('/', 'CarsController@getIndex');
	Route::post('/', 'CarsController@postIndex');
	Route::get('/add', 'CarsController@getAdd');
	Route::post('/add', 'CarsController@postAdd');
	Route::get('/edit/{id}', 'CarsController@getEdit');
	Route::post('/edit/{id}', 'CarsController@postEdit');

	Route::controllers([
		'users'					   	   => 'UsersController',
		'owners'					   => 'OwnersController',
	]);
});

Route::controllers([
	'auth'     => 'Auth\AuthController',
	'password' => 'Auth\PasswordController'
]);