<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class HomeController extends BaseController
{
    public function getIndex() {

    	$ccTitle = 'Домашняя страница';

    	return view('home', compact(['ccTitle']));
    }
}
