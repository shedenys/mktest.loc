<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class BaseController extends Controller
{
    public function __construct(){

		$ccServerHttpHost = \Request::server('HTTP_HOST');

		// Формируем дефолтные хлебные крошки
		$ccBreadcrumbs[] = [
			'url' => '',
			'name' => 'Главная'
		];
		// Делаем доступными некоторые данные для использования другими контроллерами
		$this->ccBreadcrumbs = $ccBreadcrumbs;
    	
		view()->share(compact(['ccServerHttpHost', 'ccBreadcrumbs']));
	}
}
