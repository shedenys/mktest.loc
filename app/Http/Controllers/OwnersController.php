<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\OwnersRequest;
use App\Owners;

class OwnersController extends BaseController
{
    
    public function getIndex(Request $request) {

    	$ccTitle = 'Владельцы';
    	$ccBreadcrumbs = $this->ccBreadcrumbs;
        $ccBreadcrumbs[] = [
            'name' => $ccTitle
        ];

        $owners = new Owners;

        // Sorting
        $sort = $request->sort;
        if(count($sort)) {
            foreach ($sort as $column => $order) {
                if($order != null) {
                    $owners = $owners->orderBy($column, $order);
                }
            }
        }
        else {
            $sort = [];
        }
        $nextOrder = ['0' => 'asc', 'asc' => 'desc', 'desc' => ''];

        $owners = $owners->paginate();
        $ownersTotal = Owners::count();

    	return view('showOwners', compact(['ccTitle', 'ccBreadcrumbs', 'owners', 'ownersTotal', 'cropperSettings', 'sort', 'nextOrder']));
    }

    public function postIndex(Request $request) {

        $itemsIDs = $request->get('items');
        // Если выбрано действие для удаления
        if($request->get('action') == 'delete') {

            // Удаляем владельцев
            Owners::destroy($itemsIDs);

            if(count($itemsIDs) > 1) {
                $successMessage = 'Выбранные владельцы удалены.';
            }
            else {
                $successMessage = 'Выбранный владелец удален.';
            }
            return redirect()->back()->with('success', $successMessage)->with('success-title', 'Удалено');
        }

        return redirect()->back();
    }

    public function getAdd() {

        // Хлебные крошки
    	$ccTitle = 'Добавить владельца';
    	$ccBreadcrumbs = $this->ccBreadcrumbs;
        $ccBreadcrumbs[] = [
            'url' => 'owners',
            'name' => 'Владельцы'
        ];
        $ccBreadcrumbs[] = [
            'name' => $ccTitle
        ];

        $owner = new Owners;

    	return view('editOwner', compact(['ccTitle', 'ccBreadcrumbs', 'owner']));
    }

    public function postAdd(OwnersRequest $request) {

        $owner = Owners::create($request->only(['name', 'birthday', 'home_address', 'phone']));

        return redirect('/owners')->with('success', 'Владелец успешно добавлен.');
    }

    public function getEdit($id) {

        $ccTitle = 'Редактировать владельца';
        $ccBreadcrumbs = $this->ccBreadcrumbs;
        $ccBreadcrumbs[] = [
            'url' => 'articles',
            'name' => 'Владельцы'
        ];
        $ccBreadcrumbs[] = [
            'name' => $ccTitle
        ];

        $owner = Owners::findOrFail($id);

        return view('editOwner', compact(['ccTitle', 'ccBreadcrumbs', 'owner']));
    }

    public function postEdit(OwnersRequest $request, $id) {

        $owner = $request->except('_token');

        Owners::findOrFail($id)->update($owner);

        return redirect('/owners/')->with('success', 'Владелец успешно обновлен.')->with('success-title', 'Обновлено');
    }
}
