<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\AddUserRequest;
use App\Http\Requests\EditUserRequest;
use App\Http\Controllers\Controller;
use App\User;
use Hash;
use Auth;

class UsersController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->authUser = Auth::user();
        // Запрещаем пользователям с правами ниже superadmin управлять редактировать данные других пользователей
        $this->middleware('acc.superadmin', ['except' => ['getEdit', 'postEdit']]);
    }

    public function getIndex(Request $request) {

    	$ccTitle = 'Пользователи';
    	$ccBreadcrumbs = $this->ccBreadcrumbs;
        $ccBreadcrumbs[] = [
            'name' => $ccTitle
        ];
        $users = new User;

        // Sorting
        $sort = $request->sort;
        if(count($sort)) {
            foreach ($sort as $column => $order) {
                if($order != null) {
                    $users = $users->orderBy($column, $order);
                }
            }
        }
        else {
            $sort = [];
        }
        $nextOrder = ['0' => 'asc', 'asc' => 'desc', 'desc' => ''];

        $users = $users->paginate();
        $usersTotal = User::count();

    	return view('showUsers', compact(['ccTitle', 'ccBreadcrumbs', 'users', 'usersTotal', 'sort', 'nextOrder']));
    }

    public function postIndex(Request $request) {

        $itemsIDs = $request->get('items');
        // Если выбрано действие для удаления
        if($request->get('action') == 'delete') {

             // Флаг отображения ошикби удаления зарезервированной страницы
            $reserved = false;
        	// Получаем ID пользователей с правами superadmin
            $superadminUsersInfo = User::where('role', 'superadmin')->select('id')->get();
            // Исключаем удаление зарезервированных страниц
            foreach ($superadminUsersInfo as $superadminUser) {
                if(in_array($superadminUser->id, $itemsIDs)) {
                    unset($itemsIDs[array_search($superadminUser->id, $itemsIDs)]);
                    $reserved = true;
                }
            }

            // Удаляем пользователи
            if(count($itemsIDs)) {
                User::destroy($itemsIDs);

                if(count($itemsIDs) > 1) {
                    $successMessage = 'Выбранные пользователи удалены.';
                }
                elseif($reserved == true) {
                    return redirect()->back()->with('warning', 'Удалены некоторые выбранные пользователи, кроме superadmin-пользователя, которого запрещено удалять.')->with('warning-title', 'Удалено частично');
                }
                else {
                    $successMessage = 'Выбранный пользователь удален.';
                }
                return redirect()->back()->with('success', $successMessage)->with('success-title', 'Удалено');
            }
            else {
                if($reserved == true) {
                    return redirect()->back()->with('error', 'Запрещено удалять пользователя с правами superadmin.');
                }
                else {
                    return redirect()->back();
                }
            }
            return redirect()->back()->with('success', $successMessage)->with('success-title', 'Удалено');
        }

        return redirect()->back();
    }

    public function getAdd() {

        // Хлебные крошки
    	$ccTitle = 'Добавить пользователя';
    	$ccBreadcrumbs = $this->ccBreadcrumbs;
        $ccBreadcrumbs[] = [
            'name' => $ccTitle
        ];

        $user = new User;
        $user->role = 'admin';

    	return view('editUser', compact(['ccTitle', 'ccBreadcrumbs', 'user']));
    }

    public function postAdd(AddUserRequest $request) {

		$user = User::create([
			'name'   		=> $request->get('name'),
			'email'   		=> $request->get('email'),
			'password' 		=> Hash::make($request->get('password')),
			'role'			=> 'admin'
		]);

        return redirect('/users')->with('success', 'Пользователь успешно добавлен.');
	}

	public function getEdit($id) {

        // Делаем невозможным редактировать данные другого пользователя пользователем с правами admin
        if($this->authUser->role == 'admin' && $id <> $this->authUser->id) {
            return redirect('/');
        }

        $ccTitle = 'Редактировать пользователя';
        $ccBreadcrumbs = $this->ccBreadcrumbs;
        $ccBreadcrumbs[] = [
            'name' => $ccTitle
        ];

        $user = User::findOrFail($id);

        return view('editUser', compact(['ccTitle', 'ccBreadcrumbs', 'user', 'cropperSettings']));
    }

    public function postEdit(EditUserRequest $request, $id){

		$user = User::findOrFail($id);
		$user->name = $request->get('name');
		$user->email = $request->get('email');
		if($request->get('password') != '') {
			$user->password = Hash::make($request->get('password'));
		}

		$user->save();

        if($this->authUser->role == 'admin') {
            return redirect()->back()->with('success', 'Аккаунт пользователя успешно обновлен.')->with('success-title', 'Обновлено');
        }

        return redirect('/users')->with('success', 'Аккаунт пользователя успешно обновлен.')->with('success-title', 'Обновлено');
	}
}
