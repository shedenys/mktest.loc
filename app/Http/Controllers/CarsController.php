<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\CarsRequest;
use App\Cars;
use App\Owners;

class CarsController extends BaseController
{
    
    public function getIndex(Request $request) {

    	$ccTitle = 'Автомобили';
    	$ccBreadcrumbs = $this->ccBreadcrumbs;

        $cars = Cars::leftJoin('owners', 'cars.owner_id', '=', 'owners.id')
                    ->select('cars.*', 'owners.name AS owner_name');

        // Sorting
        $sort = $request->sort;
        if(count($sort)) {
            foreach ($sort as $column => $order) {
                if($order != null) {
                    $cars = $cars->orderBy($column, $order);
                }
            }
        }
        else {
            $sort = [];
        }
        $nextOrder = ['0' => 'asc', 'asc' => 'desc', 'desc' => ''];

        $cars = $cars->paginate();
        

        $carsTotal = Cars::count();

    	return view('showCars', compact(['ccTitle', 'ccBreadcrumbs', 'cars', 'carsTotal', 'cropperSettings', 'sort', 'nextOrder']));
    }

    public function postIndex(Request $request) {

        $itemsIDs = $request->get('items');
        // Если выбрано действие для удаления
        if($request->get('action') == 'delete') {

            // Удаляем владельцев
            Cars::destroy($itemsIDs);

            if(count($itemsIDs) > 1) {
                $successMessage = 'Выбранные автомобили удалены.';
            }
            else {
                $successMessage = 'Выбранный автомобиль удален.';
            }
            return redirect()->back()->with('success', $successMessage)->with('success-title', 'Удалено');
        }

        return redirect()->back();
    }

    public function getAdd() {

        // Хлебные крошки
    	$ccTitle = 'Добавить автомобиль';
    	$ccBreadcrumbs = $this->ccBreadcrumbs;
        $ccBreadcrumbs[] = [
            'name' => $ccTitle
        ];

        $car = new Cars;
        $owners = Owners::select('id', 'name')->get();

    	return view('editCar', compact(['ccTitle', 'ccBreadcrumbs', 'car', 'owners']));
    }

    public function postAdd(CarsRequest $request) {

        $car = Cars::create($request->only(['mark', 'model', 'issue_year', 'color', 'number', 'owner_id']));

        if($car['owner_id'] == 0) {
            $car['owner_id'] = null;
        }

        return redirect('/')->with('success', 'Автомобиль успешно добавлен.');
    }

    public function getEdit($id) {

        $ccTitle = 'Редактировать автомобиль';
        $ccBreadcrumbs = $this->ccBreadcrumbs;
        $ccBreadcrumbs[] = [
            'name' => $ccTitle
        ];

        $car = Cars::findOrFail($id);

        $owners = Owners::select('id', 'name')->get();

        return view('editCar', compact(['ccTitle', 'ccBreadcrumbs', 'car', 'owners']));
    }

    public function postEdit(CarsRequest $request, $id) {

        $car = $request->except('_token');

        if($car['owner_id'] == 0) {
            $car['owner_id'] = null;
        }

        Cars::findOrFail($id)->update($car);

        return redirect('/')->with('success', 'Автомобиль успешно обновлен.')->with('success-title', 'Обновлено');
    }
}
